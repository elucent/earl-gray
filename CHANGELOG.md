# 0.4

 * Updated to 19w09a, merged pull request from B0undarybreaker (ty!)

# 0.3

Fixed:

 * Strange bug (?) regarding NoSuchMethodException on EarlGray.getPropertyPacket().

Added:

 * Hook when entities are respawned. Now traits will be properly copied over when a player is cloned, and you can
   perform custom behavior when a respawn occurs.

 * Generics support to inherents, so the functor you provide takes an inferred entity type rather than the base Entity.
   Minor, but should make it a bit more convenient.

# 0.2

Added:

 * Better use of Java generics, for more convenient library use.

# 0.1

Added:

 * *Existence*