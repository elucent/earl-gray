# Earl Gray

## What is this mod?

Earl Gray is a property/trait API for the Fabric mod loader. It features
automatically saved and synchronized traits that can contain arbitrary data
and have special independent behavior. There's a lot you can do with this 
system, so try to understand the basics and see where you can go from there!

## Defining a trait.

All traits must extend the `Trait` abstract base class, and implement the `read`
and `write` functions appropriately. From there, you can do all kinds of things.
Traits are self-contained, and every trait you apply to an entity will be its
own independent instance, so you can define any variables you want within your
trait - just make sure to save them in `read`/`write` accordingly.

If you want your trait to constantly update with the entity, you should
override the `isTickable` function within `Trait`: only when this returns true
will your trait be allowed to tick, so make sure it always does! After that, you
should also override the `update` function to perform your chosen behavior.

No matter what functions or behavior you implement for your trait, make sure
that if you ever make meaningful changes to its data, that you call
`markDirty`. This will make the entity synchronize its traits from server to
client, keeping everything up-to-date.

## Registering a trait.

Registering traits is necessary for them to be properly deserialized. We can do
this with the `TraitRegistry` class. Simply call the `register` function of the
`TraitRegistry` class with appropriate arguments during the initialization
period of your mod.

Here's an example, using the pre-defined `IntTrait` trait from the mod:
```java
static final String MODID = "test";
static TraitEntry<IntTrait> TEST_TRAIT;

@Override
public void onInitialize() {
    TEST_TRAIT = TraitRegistry.register(new Identifier(MODID, "test"), IntTrait.class);
}
```
Notice how we keep track of the trait entry we use to register the trait. This
is important! The entry a trait is basically its handle, every trait must
have exactly one unique entry. We'll want to remember it so that we can
use it to get or remove traits from entities in the future.

One thing to note is that each trait should also have its own class. This may
change in the future, but the association between trait class and entry is
a bidirectional one. So try not to reuse tiny traits like `IntTrait` over and
over, because you'd end up having to implement a separate class for each one.
Again, this may change in the near future, but for now it's worth knowing.

## Applying a trait.

Applying traits is fairly simple. With this mod installed, entities will
implement the `TraitHolder` interface, which allows us to access their
`TraitContainer`. This container is a data structure that manages all the
traits currently on an entity. To add a new trait to that entity, we can
simply call the `addTrait` function from the `TraitContainer` class. Here's an
example, using the trait we defined above:
```java
Entity entity;
entity.addTrait(new IntTrait());
```
The trait will then last until we manually remove it, or until the entity dies.

We can also automatically apply traits to entities when they spawn, since
sometimes we want to apply traits to whole entity classes instead of single
entities. This can be done using the trait registry, using "inherent" traits:
```java
TraitRegistry.addInherent(CowEntity.class, (Entity e) -> new IntTrait());
```
We have to provide both a class we want to apply the trait to, and a function
for generating the trait. The function's type should align with a
`Function<Entity, Trait>` object - I recommend using a lambda for simplicity.
This function is how the trait will be generated after the entity spawns, and
its return value will be added to the entity's trait container.

Note that the class we pass in factors in class hierarchy as well: if we were to
add an inherent trait to `LivingEntity.class`, for example, then every living
entity would be given that trait.

## Using a trait.

Traits will normally behave independently, but if we're storing data using one,
we might want to be able to read and write that data. We can use a couple
functions to work with traits. Let's say we have an entity with TEST_TRAIT as we
defined above named `testEntity`, and we want to set its trait's value to 10:
```java
((TraitHolder)testEntity).getTraits().getTrait(TEST_TRAIT).setValue(10);
```
As you can see, we can use `getTrait` to get trait instances, and call functions
on them.

There is also `hasTrait` and `removeTrait`, which will detect if a trait is
present and remove a trait from an entity respectively. Both, like `getTrait`,
require that you pass in the identifier for the trait you're looking for.

With the cast to `TraitHolder`, and the call to `getTraits`, this is a good bit
of typing - so there is also a shorthand method for each of the `TraitContainer`
functions we've discussed so far:
```java
((TraitHolder)testEntity).getTraits().getTrait(TEST_TRAIT).setValue(10);
Traits.get(testEntity, TEST_TRAIT).setValue(10);
```
Both of the above lines are equivalent.