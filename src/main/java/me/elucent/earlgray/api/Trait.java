package me.elucent.earlgray.api;

import net.minecraft.entity.Entity;
import net.minecraft.nbt.CompoundTag;

public abstract class Trait {
    TraitContainer container;

    /*
     * write()
     *
     * Returns a compound tag containing all the information necessary for
     * this trait. This is how the trait will be saved or serialized.
     * Should return the tag passed in, just with additional data written to it.
     */
    public abstract CompoundTag write(CompoundTag tag);

    /*
     * read()
     *
     * Reads the trait's data from a tag, and returns itself. This is how the
     * property will be loaded or deserialized.
     */
    public abstract Trait read(CompoundTag tag);

    /*
     * update()
     *
     * Contains any functionality this trait should do on tick. Will never
     * be called if isTickable() returns false.
     */
    public void update(Entity entity) {}

    /*
     * isTickable()
     *
     * Returns whether or not the trait is tickable. If false, update()
     * will never be called, and the trait will not be interacted with
     * automatically each tick.
     */
    public boolean isTickable() {
        return false;
    }

    /*
     * renew()
     *
     * Called when a new trait is applied to the object, while the
     * same trait type still had an instance in effect. Override this
     * function to update the active trait's contents accordingly
     * based on the new instance.
     * You are guaranteed that the trait argument will have the same
     * type as the active trait.
     */
    public void renew(Trait trait) {}

    /*
     * onRespawn()
     *
     * Called when a trait is reapplied to a respawned entity. Applies to
     * players pretty much exclusively.
     */
    public void onRespawn(Entity entity) {}

    /*
     * onAdd()
     *
     * Called when the property is newly applied to an entity - not when it
     * is renewed.
     */
    public void onAdd(Entity entity) {}

    /*
     * onRemove()
     *
     * Called when the property is about to be removed from an entity.
     */
    public void onRemove(Entity entity) {}

    /*
     * markDirty()
     *
     * Marks the enclosing TraitContainer instance dirty. This means that on
     * the next tick, this property will be synchronized with the client.
     */
    public final void markDirty() {
        container.markDirty();
    }
}
