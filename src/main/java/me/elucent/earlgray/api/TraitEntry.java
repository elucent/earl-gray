package me.elucent.earlgray.api;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.Identifier;

public class TraitEntry<T extends Trait> {
    Class<T> traitClass;
    Identifier name;

    public TraitEntry(Identifier name, Class<T> traitClass) {
        this.traitClass = traitClass;
        this.name = name;
    }

    public T generate(CompoundTag tag) {
        T t = generate();
        if (t == null) return null;
        return (T)t.read(tag);
    }

    public T generate() {
        try {
            return traitClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Identifier getName() {
        return name;
    }
}
