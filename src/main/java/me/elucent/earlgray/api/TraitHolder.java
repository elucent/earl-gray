package me.elucent.earlgray.api;

public interface TraitHolder {
    /*
     * getProperties()
     *
     * Returns a property container. Use this to interact
     * with properties.
     */
    public TraitContainer getTraits();
}
