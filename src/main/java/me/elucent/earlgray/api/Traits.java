package me.elucent.earlgray.api;

import net.minecraft.entity.Entity;
import net.minecraft.util.Identifier;

public class Traits {
    public static <T extends Trait> void add(Entity entity, T trait) {
        ((TraitHolder)entity).getTraits().addTrait(trait);
    }

    public static <T extends Trait> T get(Entity entity, TraitEntry<T> entry) {
        return ((TraitHolder)entity).getTraits().getTrait(entry);
    }

    public static boolean has(Entity entity, Trait trait) {
        return ((TraitHolder)entity).getTraits().hasTrait(trait);
    }

    public static boolean has(Entity entity, TraitEntry<? extends Trait> entry) {
        return ((TraitHolder)entity).getTraits().hasTrait(entry);
    }

    public static <T extends Trait> void remove(Entity entity, T trait) {
        ((TraitHolder)entity).getTraits().removeTrait(trait);
    }

    public static <T extends Trait> void remove(Entity entity, TraitEntry<T> entry) {
        ((TraitHolder)entity).getTraits().removeTrait(entry);
    }
}
