package me.elucent.earlgray.props;

import me.elucent.earlgray.api.Trait;
import net.minecraft.nbt.CompoundTag;

public class CompoundTrait extends Trait {
    CompoundTag tag;

    @Override
    public CompoundTag write(CompoundTag tag) {
        return this.tag;
    }

    @Override
    public Trait read(CompoundTag tag) {
        this.tag = tag;
        return this;
    }

    public CompoundTag get() {
        return tag;
    }
}
