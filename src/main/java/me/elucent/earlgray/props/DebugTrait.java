package me.elucent.earlgray.props;

import me.elucent.earlgray.api.Trait;
import me.elucent.earlgray.api.Traits;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.nbt.CompoundTag;

public class DebugTrait extends Trait {
    int val = 0;

    @Override
    public CompoundTag write(CompoundTag tag) {
        tag.putInt("val", val);
        return tag;
    }

    @Override
    public Trait read(CompoundTag tag) {
        val = tag.getInt("val");
        return this;
    }

    @Override
    public void update(Entity e) {
        if (e.age % 40 == 0) {
            System.out.println("" + val);
            val ++;
            markDirty();
        }
    }

    @Override
    public boolean isTickable() {
        return true;
    }
}
